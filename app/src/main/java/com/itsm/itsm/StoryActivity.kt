package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story.*

class StoryActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story)
        buttonView = findViewById(R.id.button_view)
        generateStory()
    }

    private fun generateStory() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }
        text_story.text = "$nameText no creía en todos los santos " +
                "$nameText vivía solito, ya no tenía mujer, pero un día $nameText se casó con una viuda, la que heredó de su\n" +
                " difunto esposo algo de bienes, pues no era muy pobre aquel difunto; por lo tanto, su mujer \n" +
                "tenía bastantes marranos, guajolotes y gallinas. Al llegar todos, el Santo le dijo a su mujer: \n" +
                " “No vas a matar nada, ni siquiera un pollo."
        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply { putString("key_name",nameText) }

            intent = Intent(this,StoryActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}