package com.itsm.itsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_story4.*

class StoryActivity4 : AppCompatActivity() {
    var name_text: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story4)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        text_story4.text = "Uno dijo: “Yo encontré mi casa muy bonita, traje mi ropa, mi pañuelo, " +
                "¿y tú?” “¿Yo?, me fue bien, me dieron todo lo que ellos tienen”. $name_text sólo seguía escuchando, y preguntaron " +
                "al otro: “A mí no me dieron nada, nomás esto me habían puesto; pero a ver " +
                "si tardan en vivir”, hablaba, y esa voz se oía con tristeza, bien se oía que " +
                "lloraba esa persona. $name_text que había ido a la milpa escuchaba todas las " +
                "palabras y oyó que era la voz del hombre que había sido marido de su mujer. " +
                "Lo que llevaba aquel difunto se oía bien que todavía estaba hirviendo y " +
                "algunos de sus compañeros le decían que lo aventara y ellos le convidaban un " +
                "poco de lo que llevaban. $name_text, al escuchar y reconocer aquella voz, marchó " +
                "para su casa."

    }
}