package com.itsm.itsm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fable4.*

class FableActivity4 : AppCompatActivity() {
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable4)
        generateFable()
    }

    private fun generateFable() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        text_fable4.text = "Moraleja: Cuanto más grande sea lo que deseamos, más grandes son " +
                "los riesgos. Y además $nameText se murió :3, pobrecita, f por $nameText"
    }
}