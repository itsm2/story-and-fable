package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story3.*

class StoryActivity3 : AppCompatActivity() {
    var name_text: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story3)
        buttonView = findViewById(R.id.button_view3)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        text_story3.text = "$name_text se fue a su milpa y la mujer fue a cortar lo’e; empezó a " +
                "guisar y al terminar puso su ofrenda en el altar. Cuando ya estaba terminado " +
                "el Todos Santos, $name_text venía solito en el camino de regreso de su milpa y " +
                "ahí por donde pasaba había otro camino que era el del camposanto. Al momento, $name_text " +
                "oyó que hablaban preguntándose unos a otros lo que llevaban."

        buttonView.setOnClickListener{
            var bundle2 = Bundle()
            bundle2.apply{ putString("key_name",name_text) }
            var intent = Intent(this,StoryActivity4::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}