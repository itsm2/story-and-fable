package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Option : AppCompatActivity() {
    var name_person: String? = null
    lateinit var buttonStory: Button
    lateinit var buttonFable: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_option)
        viewOption()
    }

    private fun viewOption() {
        buttonStory = findViewById(R.id.button_story)
        buttonFable = findViewById(R.id.button_fable)

        val bundle : Bundle? = intent.extras
        bundle?.let {
            this.name_person = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply {
            putString("key_name",name_person)
        }

        buttonStory.setOnClickListener{

            var intent = Intent(this,StoryActivity::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }

        buttonFable.setOnClickListener{

            var intent = Intent(this,FableActivity::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}