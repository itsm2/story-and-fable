package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable.*
import kotlinx.android.synthetic.main.activity_story.*
import kotlinx.android.synthetic.main.activity_story.text_story

class FableActivity : AppCompatActivity() {
    var nameText: String? = null
    lateinit var buttonView: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable)
        buttonView = findViewById(R.id.button_view_fable)
        generateFable()
    }

    private fun generateFable() {

        val bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_fable.text = "Tras mucho tiempo intentando cazar a una corneja, un hombre consiguió " +
                "al fin su premio y la llamó $nameText. El hizo algo para evitar que se escapara su tan codiciada pieza, le anudó " +
                "un filo hilo a una de sus patas y se la llevó a su hijo como regalo. A pesar de" +
                " que el hijo del hombre se desvivía por darle los mejores cuidados del mundo a $nameText, la " +
                "corneja $nameText no acababa de sentirse cómoda en su nuevo hogar."

        buttonView.setOnClickListener{
            intent = Intent(this,FableActivity2::class.java).apply { putExtras(bundle2) }
            startActivity(intent)
        }

    }
}