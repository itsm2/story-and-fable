package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_story2.*

class StoryActivity2 : AppCompatActivity() {
    lateinit var buttonView: Button
    var name_text: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story2)
        buttonView = findViewById(R.id.button_view2)
        generateStory()
    }

    private fun generateStory() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            name_text = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_text) }

        text_story2.text = "Así nomás la vamos a pasar en todos Santos, dijo $name_text, no vamos a comprar nada, no " +
                "hay dinero con qué comprar. Si hay lo que hay, ahí que estén, no es cierto que " +
                "vienen en Todos Santos los que ya han muerto. “Yo $name_text no los he visto, ¿Quién los ha visto, si es cierto " +
                "que vienen? Nomás dicen. No es cierto que vienen. ¿Cuándo van a volver si ya están " +
                "podridos?” Le dijo $name_text a su mujer: “Vas a ir a cortar lo’e y eso es lo que vas a " +
                "guisar, si quieres poner ofrenda”"

        buttonView.setOnClickListener{
            var intent = Intent(this,StoryActivity3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}