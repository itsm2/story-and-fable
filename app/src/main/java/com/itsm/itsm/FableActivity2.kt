package com.itsm.itsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_fable2.*

class FableActivity2 : AppCompatActivity() {
    lateinit var buttonView: Button
    var nameText: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fable2)
        buttonView = findViewById(R.id.button_view_fable2)
        generateFable()
    }

    private fun generateFable() {

        var bundle: Bundle? = intent.extras
        bundle?.let {
            nameText = it.getString("key_name")
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",nameText) }

        text_fable2.text = "Una tarde, mientras el hijo de del hombre limpiaba la jaula que le servía como hogar" +
                ", la corneja $nameText aprovecho que nadie la vigilaba para salir por la ventana y volar " +
                "hacia el lugar en que estaba construido su nido. Tan emocionada estaba $nameText por" +
                " recobrar su libertad, que al posarse sobre su árbol," +
                " el hilo que colgaba de una de sus patas se enredó terriblemente en varias ramas, pobrecita $nameText."

        buttonView.setOnClickListener{
            var intent = Intent(this,FableActivity3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}